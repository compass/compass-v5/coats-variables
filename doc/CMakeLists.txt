find_package(Doxygen REQUIRED dot)

set(DOXYGEN_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/doc)
doxygen_add_docs(
  doxygen
  # add the files to be parsed by doxygen here
  # ${PROJECT_SOURCE_DIR}/src/include/coats-variables/foo1.h
  # ${PROJECT_SOURCE_DIR}/src/include/coats-variables/foo2.h
  USE_STAMP_FILE)
