#include <coats_variables/sanity_check.h>
#include <icus/field-binding.h>
#include <nanobind/nanobind.h>

namespace nb = nanobind;

void bind_coats_variables(nb::module_ &module) {

  module.def("test_units_variables", &coats::test_units_variables);
  module.def("test_accumulation", &coats::test_accumulation);
  module.def("test_closure_laws", &coats::test_closure_laws);
  module.def("test_phase_comp_mobility", &coats::test_phase_comp_mobility);
  module.def("test_phase_thermal_mobility",
             &coats::test_phase_thermal_mobility);
  module.def("test_densities", &coats::test_densities);
}

NB_MODULE(bindings, module) { bind_coats_variables(module); }
