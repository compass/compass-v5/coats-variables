#pragma once
#include "pscal/state.h"
#include "pscal/value.h"
#include "pscal/variable.h"
#include <algorithm>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <physicalprop/physicalprop.h>
#include <physics/model.h>

namespace coats {
namespace pp = physical_prop;
using std::views::enumerate;

template <physics::Phase alpha> inline auto _pressure(const physics::State &x) {
  physics::State ddX{};
  ddX.pressure[alpha] = 1.0;
  return pscal::Scalar<physics::State>{x.pressure[alpha], ddX};
};

inline auto _temperature(const physics::State &x) {
  physics::State ddX{};
  ddX.temperature = 1.0;
  return pscal::Scalar<physics::State>{x.temperature, ddX};
};

template <physics::Phase alpha>
inline auto _saturation(const physics::State &x) {
  physics::State ddX{};
  ddX.saturation[alpha] = 1.0;
  return pscal::Scalar<physics::State>{x.saturation[alpha], ddX};
};

template <physics::Phase alpha>
inline auto _molar_fractions(const physics::State &x) {
  pscal::Vector<physics::State, physics::nb_components> res{};
  res.value = x.molar_fractions[alpha];
  for (auto &&[i, ddCi] : enumerate(res.derivatives)) {
    ddCi.molar_fractions[alpha][i] = 1.0;
  }
  return res;
};

// Molar internal energy = molar enthalpy - Pressure/molar density
template <physics::Phase alpha>
inline auto _molar_internal_energy(pp::FMP_d &fluid_properties) {
  return pp::make_variable<alpha>(fluid_properties.molar_enthalpy(alpha)) -
         pp::make_variable_IdState(_pressure<alpha>) /
             pp::make_variable<alpha>(fluid_properties.molar_density(alpha));
}

// phase pressure
template <physics::Phase... ph>
inline auto phase_pressure_functors(physics::Phase_set<ph...>) {
  return physics::phase_functors(pp::make_variable_IdState(_pressure<ph>)...);
}
inline auto init_phase_pressure_functors() {
  return phase_pressure_functors(physics::all_phases{});
}
using Phase_pressure_funct_t =
    std::invoke_result_t<decltype(init_phase_pressure_functors)>;

inline auto init_temperature_functor() {
  return pp::make_variable_IdState(_temperature);
}
using Phase_temperature_funct_t =
    std::invoke_result_t<decltype(init_temperature_functor)>;

// phase saturation
template <physics::Phase... ph>
inline auto phase_saturation_functors(physics::Phase_set<ph...>) {
  return physics::phase_functors(pp::make_variable_IdState(_saturation<ph>)...);
}
inline auto init_phase_saturation_functors() {
  return phase_saturation_functors(physics::all_phases{});
}
using Phase_sat_funct_t =
    std::invoke_result_t<decltype(init_phase_saturation_functors)>;

template <physics::Phase... ph>
inline auto phase_saturation_functors_no_d(physics::Phase_set<ph...>) {
  return physics::phase_functors(
      ([](const physics::State &x) { return x.saturation[ph]; })...);
}
inline auto init_phase_saturation_functors_no_d() {
  return phase_saturation_functors_no_d(physics::all_phases{});
}
using Phase_sat_funct_no_d_t =
    std::invoke_result_t<decltype(init_phase_saturation_functors_no_d)>;

// phase molar fractions
template <physics::Phase... ph>
inline auto phase_molar_fractions_functors(physics::Phase_set<ph...>) {
  return physics::phase_functors(
      pp::make_vect_variable_IdState(_molar_fractions<ph>)...);
}
inline auto init_phase_molar_fractions_functors() {
  return phase_molar_fractions_functors(physics::all_phases{});
}
using Phase_molar_frac_funct_t =
    std::invoke_result_t<decltype(init_phase_molar_fractions_functors)>;

// phase volumetric mass densities
template <physics::Phase... ph>
inline auto phase_density_functors(physics::Phase_set<ph...>,
                                   pp::FMP_d &fluid_properties) {
  return physics::phase_functors(
      pp::make_variable<ph>(fluid_properties.density(ph))...);
}
inline auto init_phase_density_functors(pp::FMP_d &fluid_properties) {
  return phase_density_functors(physics::all_phases{}, fluid_properties);
}
using Phase_density_funct_t =
    std::invoke_result_t<decltype(init_phase_density_functors), pp::FMP_d &>;

// molar dens * enthalpy * kr / visco
template <physics::Phase... ph>
inline auto phase_thermal_mobility_functors_rkt(physics::Phase_set<ph...>,
                                                int rocktype,
                                                pp::FMP_d &fluid_properties,
                                                pp::RP_d &rock_properties) {
  return physics::phase_functors(
      pp::make_variable<ph>(fluid_properties.molar_density(ph)) *
      pp::make_variable<ph>(fluid_properties.molar_enthalpy(ph)) /
      pp::make_variable<ph>(fluid_properties.viscosity(ph)) *
      pp::make_sat_variable(rock_properties.relative_perm(rocktype, ph))...);
}

inline auto init_phase_thermal_mobility_functors_rkt(
    int rocktype, pp::FMP_d &fluid_properties, pp::RP_d &rock_properties) {
  return phase_thermal_mobility_functors_rkt(physics::all_phases{}, rocktype,
                                             fluid_properties, rock_properties);
}
using phase_thermal_funct_t =
    std::invoke_result_t<decltype(init_phase_thermal_mobility_functors_rkt),
                         int, pp::FMP_d &, pp::RP_d &>;

inline auto init_phase_thermal_mobility_functors(pp::FMP_d &fluid_properties,
                                                 pp::RP_d &rock_properties) {
  auto &rkts = rock_properties._rel_perm_rocktypes;
  std::sort(rkts.begin(), rkts.end());
  // auto &other = rock_properties._pc_rocktypes;
  // std::sort(other.begin(), other.end());
  // assert(rkts == other);
  std::vector<phase_thermal_funct_t> phase_mob_e;
  for (auto &&[i, rkt] : enumerate(rkts)) {
    assert(i == rkt && "rocktypes must be contigues nombres");
    phase_mob_e.emplace_back(init_phase_thermal_mobility_functors_rkt(
        rkt, fluid_properties, rock_properties));
  }
  return phase_mob_e;
}

// molar dens * molar comp * kr / visco
template <physics::Phase... ph>
inline auto phase_comp_mobility_functors_rkt(physics::Phase_set<ph...>,
                                             int rocktype,
                                             pp::FMP_d &fluid_properties,
                                             pp::RP_d &rock_properties) {
  return physics::phase_functors(
      pp::make_variable<ph>(fluid_properties.molar_density(ph)) /
      pp::make_variable<ph>(fluid_properties.viscosity(ph)) *
      pp::make_sat_variable(rock_properties.relative_perm(rocktype, ph)) *
      pp::make_vect_variable_IdState(_molar_fractions<ph>)...);
}

inline auto init_phase_comp_mobility_functors_rkt(int rocktype,
                                                  pp::FMP_d &fluid_properties,
                                                  pp::RP_d &rock_properties) {
  return phase_comp_mobility_functors_rkt(physics::all_phases{}, rocktype,
                                          fluid_properties, rock_properties);
}
using phase_comp_funct_t =
    std::invoke_result_t<decltype(init_phase_comp_mobility_functors_rkt), int,
                         pp::FMP_d &, pp::RP_d &>;

inline auto init_phase_comp_mobility_functors(pp::FMP_d &fluid_properties,
                                              pp::RP_d &rock_properties) {
  auto &rkts = rock_properties._rel_perm_rocktypes;
  std::sort(rkts.begin(), rkts.end());
  // auto &other = rock_properties._pc_rocktypes;
  // std::sort(other.begin(), other.end());
  // assert(rkts == other);
  std::vector<phase_comp_funct_t> phase_mob_i;
  for (auto &&[i, rkt] : enumerate(rkts)) {
    assert(i == rkt && "rocktypes must be contiguous numbers");
    phase_mob_i.emplace_back(init_phase_comp_mobility_functors_rkt(
        rkt, fluid_properties, rock_properties));
  }
  return phase_mob_i;
}

// zeta_i: molar dens * saturation * molar fractions
template <physics::Phase... ph>
inline auto phase_volumic_acc_functors(physics::Phase_set<ph...>,
                                       pp::FMP_d &properties) {
  return physics::phase_functors(
      pp::make_variable<ph>(properties.molar_density(ph)) *
      pp::make_variable_IdState(_saturation<ph>) *
      pp::make_vect_variable_IdState(_molar_fractions<ph>)...);
}

inline auto init_phase_volumic_acc_functors(pp::FMP_d &properties) {
  return phase_volumic_acc_functors(physics::all_phases{}, properties);
}
using Molar_acc_funct_t =
    std::invoke_result_t<decltype(init_phase_volumic_acc_functors),
                         pp::FMP_d &>;

// phase_energy: molar dens * internal energy * saturation
template <physics::Phase... ph>
inline auto phase_thermal_acc_functors(physics::Phase_set<ph...>,
                                       pp::FMP_d &properties) {
  return physics::phase_functors(
      pp::make_variable<ph>(properties.molar_density(ph)) *
      _molar_internal_energy<ph>(properties) *
      pp::make_variable_IdState(_saturation<ph>)...);
}

inline auto init_phase_thermal_acc_functors(pp::FMP_d &properties) {
  return phase_thermal_acc_functors(physics::all_phases{}, properties);
}

using Therm_acc_funct_t =
    std::invoke_result_t<decltype(init_phase_thermal_acc_functors),
                         pp::FMP_d &>;

inline auto init_rock_acc_functor(pp::RP_d &rock_properties) {
  return rock_properties.rock_heat_capacity *
         pp::make_variable_IdState(_temperature);
}

using Rock_acc_funct_t =
    std::invoke_result_t<decltype(init_rock_acc_functor), pp::RP_d &>;

// pref2palpha such that palpha = pref + pref2palpha with pref = pg
template <physics::Phase... ph>
inline auto phase_pref2palpha_functors_rkt(physics::Phase_set<ph...>,
                                           int rocktype,
                                           pp::RP_d &rock_properties) {
  return physics::phase_functors(
      pp::make_sat_variable(rock_properties.pref2palpha(rocktype, ph))...);
}

inline auto init_phase_pref2palpha_functors_rkt(int rocktype,
                                                pp::RP_d &rock_properties) {
  return phase_pref2palpha_functors_rkt(physics::all_phases{}, rocktype,
                                        rock_properties);
}
using phase_pref2palpha_funct_t =
    std::invoke_result_t<decltype(init_phase_pref2palpha_functors_rkt), int,
                         pp::RP_d &>;

inline auto init_phase_pref2palpha_functors(pp::RP_d &rock_properties) {
  auto &rkts = rock_properties._pc_rocktypes;
  std::sort(rkts.begin(), rkts.end());
  // auto &other = rock_properties._rel_perm_rocktypes;
  // std::sort(other.begin(), other.end());
  // assert(rkts == other);
  std::vector<phase_pref2palpha_funct_t> phase_pref2palpha;
  for (auto &&[i, rkt] : enumerate(rkts)) {
    assert(i == rkt && "rocktypes must be contigues nombres");
    phase_pref2palpha.emplace_back(
        init_phase_pref2palpha_functors_rkt(rkt, rock_properties));
  }
  return phase_pref2palpha;
}

// psat
inline auto init_psat(pp::FMP_d &fluid_properties) {
  return pp::make_temp_variable(fluid_properties.psat());
}

template <physics::Phase... ph>
inline auto phase_fugacity_coef_functors(physics::Phase_set<ph...>,
                                         pp::FMP_d &fluid_properties) {
  return physics::phase_functors(pp::make_vect_variable<ph>(
      _fugacity_coefficients<ph>(fluid_properties))...);
}
template <physics::Phase... ph>
inline auto phase_fugacity_coef_functors(physics::Phase_set<ph...>,
                                         pp::FMP &fluid_properties) {
  return physics::phase_functors(
      _fugacity_coefficients<ph>(fluid_properties)...);
}
template <typename Fluid_properties>
inline auto
init_phase_fugacity_coef_functors(Fluid_properties &fluid_properties) {
  return phase_fugacity_coef_functors(physics::all_phases{}, fluid_properties);
}

using Phase_fugacity_coef_funct_t =
    std::invoke_result_t<decltype(init_phase_fugacity_coef_functors<pp::FMP_d>),
                         pp::FMP_d &>;

using Phase_fugacity_coef_funct_no_d_t =
    std::invoke_result_t<decltype(init_phase_fugacity_coef_functors<pp::FMP>),
                         pp::FMP &>;

template <physics::Phase... ph>
inline auto phase_fugacity_functors(physics::Phase_set<ph...>,
                                    pp::FMP_d &fluid_properties) {
  return physics::phase_functors(
      pp::make_vect_variable<ph>(_fugacity<ph>(fluid_properties))...);
}
template <physics::Phase... ph>
inline auto phase_fugacity_functors(physics::Phase_set<ph...>,
                                    pp::FMP &fluid_properties) {
  return physics::phase_functors(_fugacity<ph>(fluid_properties)...);
}
template <typename Fluid_properties>
inline auto init_phase_fugacity_functors(Fluid_properties &fluid_properties) {
  return phase_fugacity_functors(physics::all_phases{}, fluid_properties);
}

using Phase_fugacity_funct_t =
    std::invoke_result_t<decltype(init_phase_fugacity_functors<pp::FMP_d>),
                         pp::FMP_d &>;

using Phase_fugacity_funct_no_d_t =
    std::invoke_result_t<decltype(init_phase_fugacity_functors<pp::FMP>),
                         pp::FMP &>;

} // namespace coats
