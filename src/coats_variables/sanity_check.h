#pragma once
#include <cassert>
#include <cmath>
#include <coats_variables/coats-variables.h>
#include <iostream>

#include <physics/model.h>

namespace coats {

inline auto test_units_variables(const physics::State_field &coats_states) {
  auto eps = 1e-7;

  // construct the pressure functor (one value by phase):
  auto &&pressures = init_phase_pressure_functors();
  for (auto &&alpha : physics::all_phases::array) {
    auto &&[res_p, ddXres_p] = pressures.apply(alpha, coats_states(0));
    assert(fabs(res_p - 1.e5) < eps);
    assert(fabs(ddXres_p.temperature) < eps);
    for (auto &&dalpha : physics::all_phases::array) {
      assert(fabs(ddXres_p.saturation[dalpha]) < eps);
      for (auto &&dcomp : physics::all_components::array) {
        assert(fabs(ddXres_p.molar_fractions[dalpha][dcomp]) < eps);
      }
      if (alpha == dalpha) {
        assert(fabs(ddXres_p.pressure[dalpha] - 1.0) < eps);
      } else {
        assert(fabs(ddXres_p.pressure[dalpha]) < eps);
      }
    }
  }

  // construct the temperature functor:
  auto &&temperature_f = init_temperature_functor();
  auto &&[res_T, ddXres_T] = temperature_f(coats_states(0));
  assert(fabs(res_T - 300.0) < eps);
  assert(fabs(ddXres_T.temperature - 1.0) < eps);
  for (auto &&dalpha : physics::all_phases::array) {
    assert(fabs(ddXres_T.saturation[dalpha]) < eps);
    for (auto &&dcomp : physics::all_components::array) {
      assert(fabs(ddXres_T.molar_fractions[dalpha][dcomp]) < eps);
    }
    assert(fabs(ddXres_T.pressure[dalpha]) < eps);
  }

  return 0;
}

inline void test_accumulation(const physics::State_field &coats_states,
                              const physics::Context_field &coats_contexts,
                              pp::FMP_d &fluid_properties,
                              pp::RP_d &rock_properties,
                              const physics::Scalar_field &porosity) {
  assert(icus::have_same_support(coats_states, porosity, coats_contexts));
  assert(porosity == 0.15);
  assert(coats_contexts == physics::Context::gas);
  auto eps = 1e-7;

  auto water_component = physics::component_index(physics::Component::water);
  auto air_component = physics::component_index(physics::Component::air);

  // construct the functor of Phase molar acc (one value by phase):
  //  molar_dens * sat * molar_fractions
  auto &&phase_zeta_i = init_phase_volumic_acc_functors(fluid_properties);
  // gas context
  auto &&Sgas = physics::sum_on_present_phases(physics::Context::gas,
                                               phase_zeta_i, coats_states(0));
  auto air_gas = 0.4 * 0.9 * 1.e5 / 8.314 / 300;
  auto water_gas = 0.4 * 0.1 * 1.e5 / 8.314 / 300;
  auto dtemp_air_gas = -0.4 * 0.9 * 1.e5 / 8.314 / std::pow(300, 2);
  auto dtemp_water_gas = -0.4 * 0.1 * 1.e5 / 8.314 / std::pow(300, 2);
  auto dsat_air_gas = 0.9 * 1.e5 / 8.314 / 300;
  auto dsat_water_gas = 0.1 * 1.e5 / 8.314 / 300;
  auto dp_air_gas = 0.4 * 0.9 / 8.314 / 300;
  auto dp_water_gas = 0.4 * 0.1 / 8.314 / 300;
  auto dc_gas = 0.4 * 1.e5 / 8.314 / 300;
  assert(fabs(Sgas.value[air_component] - air_gas) < eps);
  assert(fabs(Sgas.value[water_component] - water_gas) < eps);
  assert(fabs(Sgas.derivatives[air_component].temperature - dtemp_air_gas) <
         eps);
  assert(fabs(Sgas.derivatives[water_component].temperature - dtemp_water_gas) <
         eps);
  assert(fabs(Sgas.derivatives[air_component].saturation[physics::Phase::gas] -
              dsat_air_gas) < eps);
  assert(
      fabs(Sgas.derivatives[water_component].saturation[physics::Phase::gas] -
           dsat_water_gas) < eps);
  assert(fabs(Sgas.derivatives[air_component].pressure[physics::Phase::gas] -
              dp_air_gas) < eps);
  assert(fabs(Sgas.derivatives[water_component].pressure[physics::Phase::gas] -
              dp_water_gas) < eps);
  for (size_t comp = 0; comp < physics::nb_components; ++comp) {
    assert(fabs(Sgas.derivatives[comp].saturation[physics::Phase::liquid]) <
           eps);
    assert(fabs(Sgas.derivatives[comp].pressure[physics::Phase::liquid]) < eps);
    assert(
        fabs(Sgas.derivatives[comp].molar_fractions[physics::Phase::gas][comp] -
             dc_gas) < eps);
    assert(fabs(Sgas.derivatives[comp]
                    .molar_fractions[physics::Phase::liquid][comp]) < eps);
  }
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(Sgas.derivatives[air_component]
                    .molar_fractions[alpha][physics::Component::water]) < eps);
    assert(fabs(Sgas.derivatives[water_component]
                    .molar_fractions[alpha][physics::Component::air]) < eps);
  }
  // liquid context
  auto &&Sliq = physics::sum_on_present_phases(physics::Context::liquid,
                                               phase_zeta_i, coats_states(1));
  auto air_liq = 0.6 * 0.2 * 1.e3 / 18.e-3;
  auto water_liq = 0.6 * 0.8 * 1.e3 / 18.e-3;
  auto dsat_air_liq = 0.2 * 1.e3 / 18.e-3;
  auto dsat_water_liq = 0.8 * 1.e3 / 18.e-3;
  auto dc_liq = 0.6 * 1.e3 / 18.e-3;
  assert(fabs(Sliq.value[air_component] - air_liq) < eps);
  assert(fabs(Sliq.value[water_component] - water_liq) < eps);
  assert(
      fabs(Sliq.derivatives[air_component].saturation[physics::Phase::liquid] -
           dsat_air_liq) < eps);
  assert(
      fabs(
          Sliq.derivatives[water_component].saturation[physics::Phase::liquid] -
          dsat_water_liq) < eps);
  for (size_t comp = 0; comp < physics::nb_components; ++comp) {
    assert(fabs(Sliq.derivatives[comp].temperature) < eps);
    assert(fabs(Sliq.derivatives[comp].saturation[physics::Phase::gas]) < eps);
    assert(fabs(Sliq.derivatives[comp]
                    .molar_fractions[physics::Phase::liquid][comp] -
                dc_liq) < eps);
    assert(fabs(Sliq.derivatives[comp]
                    .molar_fractions[physics::Phase::gas][comp]) < eps);
    for (auto &&alpha : physics::all_phases::array) {
      assert(fabs(Sliq.derivatives[comp].pressure[alpha]) < eps);
    }
  }
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(Sliq.derivatives[air_component]
                    .molar_fractions[alpha][physics::Component::water]) < eps);
    assert(fabs(Sliq.derivatives[water_component]
                    .molar_fractions[alpha][physics::Component::air]) < eps);
  }
  // diphasic context
  auto &&Sdiph = physics::sum_on_present_phases(physics::Context::diphasic,
                                                phase_zeta_i, coats_states(1));
  auto air_diph = air_gas + air_liq;
  auto water_diph = water_gas + water_liq;
  assert(fabs(Sdiph.value[air_component] - air_diph) < eps);
  assert(fabs(Sdiph.value[water_component] - water_diph) < eps);
  for (size_t comp = 0; comp < physics::nb_components; ++comp) {
    assert(fabs(Sdiph.derivatives[comp].temperature -
                Sgas.derivatives[comp].temperature -
                Sliq.derivatives[comp].temperature) < eps);
    for (auto &&alpha : physics::all_phases::array) {
      assert(fabs(Sdiph.derivatives[comp].saturation[alpha] -
                  Sgas.derivatives[comp].saturation[alpha] -
                  Sliq.derivatives[comp].saturation[alpha]) < eps);
      for (auto &&dcomp : physics::all_components::array) {
        assert(fabs(Sdiph.derivatives[comp].molar_fractions[alpha][dcomp] -
                    Sgas.derivatives[comp].molar_fractions[alpha][dcomp] -
                    Sliq.derivatives[comp].molar_fractions[alpha][dcomp]) <
               eps);
      }
    }
  }

  auto previous_acc = physics::Acc_field{coats_states.support};
  size_t i = 0;
  for (auto &&[poro, state] : std::views::zip(porosity, coats_states)) {
    auto &&acc = poro * physics::sum_on_present_phases(
                            physics::Context::diphasic, phase_zeta_i, state);
    previous_acc(i++).molar = acc.value;
  }

  // construct the functor of Phase energy acc (one value by phase):
  //  molar dens * internal energy * saturation
  auto &&phase_energy = init_phase_thermal_acc_functors(fluid_properties);
  auto &&Sgas_energy = physics::sum_on_present_phases(
      physics::Context::gas, phase_energy, coats_states(0));

  auto M_air = 29.0e-3;
  auto M_H2O = 18.0e-3;
  auto CpGas = 1000.0;
  auto Ts = 300. / 100.0;
  auto ss = 1990.89e3 + 190.16e3 * Ts + (-1.91264e3) * std::pow(Ts, 2.0) +
            0.2997e3 * std::pow(Ts, 3.0);
  auto dssdT = (190.16e3 + 2.0 * (-1.91264e3) * Ts +
                3.0 * 0.2997e3 * std::pow(Ts, 2.0)) /
               100.0;
  auto h_g = 0.9 * CpGas * M_air * 300. + 0.1 * M_H2O * ss;
  auto ddTh_g = 0.9 * CpGas * M_air + 0.1 * M_H2O * dssdT;

  auto phase_energy_res = (h_g / 8.314 / 300. - 1.) * 1.e5 * 0.4;
  auto dtemp_phase =
      (ddTh_g * 300. - h_g) / 8.314 / std::pow(300., 2) * 1.e5 * 0.4;
  auto dp_phase = (h_g / 8.314 / 300. - 1.) * 0.4;
  auto dsat_phase = (h_g / 8.314 / 300. - 1.) * 1.e5;
  auto dc_a = CpGas * M_air * 300. / 8.314 / 300. * 1.e5 * 0.4;
  auto dc_w = M_H2O * ss / 8.314 / 300. * 1.e5 * 0.4;
  assert(fabs(Sgas_energy.value - phase_energy_res) < eps);
  assert(fabs(Sgas_energy.derivatives.temperature - dtemp_phase) < eps);
  assert(fabs(Sgas_energy.derivatives.pressure[physics::Phase::gas] -
              dp_phase) < eps);
  assert(fabs(Sgas_energy.derivatives.pressure[physics::Phase::liquid]) < eps);
  assert(fabs(Sgas_energy.derivatives.saturation[physics::Phase::gas] -
              dsat_phase) < eps);
  assert(fabs(Sgas_energy.derivatives.saturation[physics::Phase::liquid]) <
         eps);
  assert(fabs(Sgas_energy.derivatives.molar_fractions[physics::Phase::gas]
                                                     [physics::Component::air] -
              dc_a) < eps);
  assert(
      fabs(Sgas_energy.derivatives.molar_fractions[physics::Phase::gas]
                                                  [physics::Component::water] -
           dc_w) < eps);
  for (auto &&comp : physics::all_components::array) {
    assert(fabs(Sgas_energy.derivatives
                    .molar_fractions[physics::Phase::liquid][comp]) < eps);
  }

  auto &&Sliq_energy = physics::sum_on_present_phases(
      physics::Context::liquid, phase_energy, coats_states(0));

  auto TdegC = 300. - 273.0;
  ss = (-14.4319e3) + 4.70915e3 * TdegC + (-4.87534) * std::pow(TdegC, 2.0) +
       1.45008e-2 * std::pow(TdegC, 3.0);
  dssdT = 4.70915e3 + 2.0 * (-4.87534) * TdegC +
          3.0 * 1.45008e-2 * std::pow(TdegC, 2.0);
  auto h_l = M_H2O * ss;

  phase_energy_res = (1000.0 / 18.0e-3 * h_l - 1.e5) * 0.6;
  dtemp_phase = 1000.0 / 18.0e-3 * M_H2O * dssdT * 0.6;
  dp_phase = -0.6;
  dsat_phase = 1000.0 / 18.0e-3 * h_l - 1.e5;
  assert(fabs(Sliq_energy.value - phase_energy_res) < eps);
  assert(fabs(Sliq_energy.derivatives.temperature - dtemp_phase) < eps);
  assert(fabs(Sliq_energy.derivatives.pressure[physics::Phase::liquid] -
              dp_phase) < eps);
  assert(fabs(Sliq_energy.derivatives.pressure[physics::Phase::gas]) < eps);
  assert(fabs(Sliq_energy.derivatives.saturation[physics::Phase::liquid] -
              dsat_phase) < eps);
  assert(fabs(Sliq_energy.derivatives.saturation[physics::Phase::gas]) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    for (auto &&comp : physics::all_components::array) {
      assert(fabs(Sliq_energy.derivatives.molar_fractions[alpha][comp]) < eps);
    }
  }

  // test rock accumulation
  auto &&rock_energy = init_rock_acc_functor(rock_properties);
  auto &&rock_acc = rock_energy(coats_states(0));
  assert(fabs(rock_acc.value - 1.6e6 * 300.) < eps);
  assert(fabs(rock_acc.derivatives.temperature - 1.6e6) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(rock_acc.derivatives.pressure[alpha]) < eps);
    assert(fabs(rock_acc.derivatives.saturation[alpha]) < eps);
    for (auto &&comp : physics::all_components::array) {
      assert(fabs(rock_acc.derivatives.molar_fractions[alpha][comp]) < eps);
    }
  }

  std::cout << "end of test" << std::endl;
}

inline auto
test_phase_comp_mobility(const physics::State_field &coats_states,
                         const physics::Context_field &coats_contexts,
                         const physics::Rocktype_field &rocktypes,
                         pp::FMP_d &fluid_properties,
                         pp::RP_d &rock_properties) {
  assert(icus::have_same_support(coats_states, rocktypes, coats_contexts));
  assert(coats_contexts == physics::Context::liquid);
  assert(rocktypes == 0);
  auto eps = 1e-7;
  auto water_component = physics::component_index(physics::Component::water);
  auto air_component = physics::component_index(physics::Component::air);

  auto phase_mob_i =
      init_phase_comp_mobility_functors(fluid_properties, rock_properties);

  auto &&[mob_g, ddXmobg] = physics::sum_on_present_phases(
      physics::Context::gas, phase_mob_i[rocktypes(1)], coats_states(1));

  auto mob_res = 0.16 * 1.e5 / 8.314 / 300. / 2.e-5;
  auto dsat = 0.8 * 1.e5 / 8.314 / 300. / 2.e-5;
  auto dtemp = -0.16 * 1.e5 / 8.314 / std::pow(300., 2) / 2.e-5;
  auto dp = 0.16 / 8.314 / 300. / 2.e-5;
  auto dc = 0.16 * 1.e5 / 8.314 / 300. / 2.e-5;
  assert(fabs(mob_g[air_component] - 0.9 * mob_res) < eps);
  assert(fabs(mob_g[water_component] - 0.1 * mob_res) < eps);
  assert(fabs(ddXmobg[air_component].saturation[physics::Phase::gas] -
              0.9 * dsat) < eps);
  assert(fabs(ddXmobg[water_component].saturation[physics::Phase::gas] -
              0.1 * dsat) < eps);
  assert(fabs(ddXmobg[air_component].saturation[physics::Phase::liquid]) < eps);
  assert(fabs(ddXmobg[water_component].saturation[physics::Phase::liquid]) <
         eps);
  assert(fabs(ddXmobg[air_component].temperature - 0.9 * dtemp) < eps);
  assert(fabs(ddXmobg[water_component].temperature - 0.1 * dtemp) < eps);
  assert(fabs(ddXmobg[air_component].pressure[physics::Phase::gas] - 0.9 * dp) <
         eps);
  assert(fabs(ddXmobg[water_component].pressure[physics::Phase::gas] -
              0.1 * dp) < eps);
  assert(fabs(ddXmobg[air_component].pressure[physics::Phase::liquid]) < eps);
  assert(fabs(ddXmobg[water_component].pressure[physics::Phase::liquid]) < eps);
  for (auto &&comp : physics::all_components::array) {
    assert(fabs(ddXmobg[air_component]
                    .molar_fractions[physics::Phase::liquid][comp]) < eps);
    assert(fabs(ddXmobg[water_component]
                    .molar_fractions[physics::Phase::liquid][comp]) < eps);
  }
  assert(fabs(ddXmobg[air_component].molar_fractions[physics::Phase::gas]
                                                    [physics::Component::air] -
              dc) < eps);
  assert(
      fabs(ddXmobg[air_component].molar_fractions[physics::Phase::gas]
                                                 [physics::Component::water]) <
      eps);
  assert(
      fabs(ddXmobg[water_component]
               .molar_fractions[physics::Phase::gas][physics::Component::air]) <
      eps);
  assert(
      fabs(ddXmobg[water_component].molar_fractions[physics::Phase::gas]
                                                   [physics::Component::water] -
           dc) < eps);

  auto &&[mob_l, ddXmobl] = physics::sum_on_present_phases(
      physics::Context::liquid, phase_mob_i[rocktypes(0)], coats_states(0));
  mob_res = 0.36 * 1.e3 / 18.e-3 / 1.e-3;
  dsat = 1.2 * 1.e3 / 18.e-3 / 1.e-3;
  dc = 0.36 * 1.e3 / 18.e-3 / 1.e-3;
  assert(fabs(mob_l[air_component] - 0.2 * mob_res) < eps);
  assert(fabs(mob_l[water_component] - 0.8 * mob_res) < eps);
  assert(fabs(ddXmobl[air_component].saturation[physics::Phase::liquid] -
              0.2 * dsat) < eps);
  assert(fabs(ddXmobl[water_component].saturation[physics::Phase::liquid] -
              0.8 * dsat) < eps);
  assert(fabs(ddXmobl[air_component].saturation[physics::Phase::gas]) < eps);
  assert(fabs(ddXmobl[water_component].saturation[physics::Phase::gas]) < eps);
  assert(fabs(ddXmobl[air_component].temperature) < eps);
  assert(fabs(ddXmobl[water_component].temperature) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXmobl[air_component].pressure[alpha]) < eps);
    assert(fabs(ddXmobl[water_component].pressure[alpha]) < eps);
  }
  for (auto &&comp : physics::all_components::array) {
    assert(fabs(ddXmobl[air_component]
                    .molar_fractions[physics::Phase::gas][comp]) < eps);
    assert(fabs(ddXmobl[water_component]
                    .molar_fractions[physics::Phase::gas][comp]) < eps);
  }
  assert(fabs(ddXmobl[air_component].molar_fractions[physics::Phase::liquid]
                                                    [physics::Component::air] -
              dc) < eps);
  assert(
      fabs(ddXmobl[air_component].molar_fractions[physics::Phase::liquid]
                                                 [physics::Component::water]) <
      eps);
  assert(
      fabs(ddXmobl[water_component].molar_fractions[physics::Phase::liquid]
                                                   [physics::Component::air]) <
      eps);
  assert(
      fabs(ddXmobl[water_component].molar_fractions[physics::Phase::liquid]
                                                   [physics::Component::water] -
           dc) < eps);

  auto &&[mob_d, ddXmobd] = physics::sum_on_present_phases(
      physics::Context::diphasic, phase_mob_i[rocktypes(0)], coats_states(0));
  assert(fabs(mob_d[air_component] - mob_g[air_component] -
              mob_l[air_component]) < eps);
  assert(fabs(mob_d[water_component] - mob_g[water_component] -
              mob_l[water_component]) < eps);
  assert(fabs(ddXmobd[air_component].temperature -
              ddXmobg[air_component].temperature -
              ddXmobl[air_component].temperature) < eps);
  assert(fabs(ddXmobd[water_component].temperature -
              ddXmobg[water_component].temperature -
              ddXmobl[water_component].temperature) < eps);

  std::cout << "end of test " << std::endl;

  return 0;
}

// molar dens * enthalpy * kr / visco
inline auto
test_phase_thermal_mobility(const physics::State_field &coats_states,
                            const physics::Context_field &coats_contexts,
                            const physics::Rocktype_field &rocktypes,
                            pp::FMP_d &fluid_properties,
                            pp::RP_d &rock_properties) {
  assert(icus::have_same_support(coats_states, rocktypes, coats_contexts));
  assert(coats_contexts == physics::Context::liquid);
  assert(rocktypes == 0);
  auto eps = 1e-7;
  auto water_component = physics::component_index(physics::Component::water);
  auto air_component = physics::component_index(physics::Component::air);
  auto M_air = 29.0e-3;
  auto M_H2O = 18.0e-3;
  auto CpGas = 1000.0;

  auto &&phase_mob_e =
      init_phase_thermal_mobility_functors(fluid_properties, rock_properties);

  auto &&[mob_g, ddXmobg] = physics::sum_on_present_phases(
      physics::Context::gas, phase_mob_e[rocktypes(1)], coats_states(1));

  auto Ts = 300. / 100.0;
  auto ss = 1990.89e3 + 190.16e3 * Ts + (-1.91264e3) * std::pow(Ts, 2.0) +
            0.2997e3 * std::pow(Ts, 3.0);
  auto dssdT = (190.16e3 + 2.0 * (-1.91264e3) * Ts +
                3.0 * 0.2997e3 * std::pow(Ts, 2.0)) /
               100.0;
  auto h_g = 0.9 * CpGas * M_air * 300. + 0.1 * M_H2O * ss;
  auto ddTh_g = 0.9 * CpGas * M_air + 0.1 * M_H2O * dssdT;

  auto mob_res = h_g * 1.e5 / 8.314 / 300. / 2.e-5 * 0.16;
  auto dsat = h_g * 1.e5 / 8.314 / 300. / 2.e-5 * 0.8;
  auto dtemp =
      1.e5 / 8.314 / 2.e-5 * 0.16 * (ddTh_g / 300. - h_g / std::pow(300., 2));
  auto dp = h_g / 8.314 / 300. / 2.e-5 * 0.16;
  auto dc_a = CpGas * M_air * 300. * 1.e5 / 8.314 / 300. / 2.e-5 * 0.16;
  auto dc_w = M_H2O * ss * 1.e5 / 8.314 / 300. / 2.e-5 * 0.16;
  assert(fabs((mob_g - mob_res) / mob_res) < eps);
  assert(fabs((ddXmobg.saturation[physics::Phase::gas] - dsat) / dsat) < eps);
  assert(fabs(ddXmobg.saturation[physics::Phase::liquid]) < eps);
  assert(fabs((ddXmobg.temperature - dtemp) / dtemp) < eps);
  assert(fabs((ddXmobg.pressure[physics::Phase::gas] - dp) / dp) < eps);
  assert(fabs(ddXmobg.pressure[physics::Phase::liquid]) < eps);
  assert(fabs((ddXmobg.molar_fractions[physics::Phase::gas]
                                      [physics::Component::air] -
               dc_a) /
              dc_a) < eps);
  assert(fabs((ddXmobg.molar_fractions[physics::Phase::gas]
                                      [physics::Component::water] -
               dc_w) /
              dc_w) < eps);
  for (auto &&comp : physics::all_components::array) {
    assert(fabs(ddXmobg.molar_fractions[physics::Phase::liquid][comp]) < eps);
  }

  auto &&[mob_l, ddXmobl] = physics::sum_on_present_phases(
      physics::Context::liquid, phase_mob_e[rocktypes(0)], coats_states(0));

  auto TdegC = 300. - 273.0;
  ss = (-14.4319e3) + 4.70915e3 * TdegC + (-4.87534) * std::pow(TdegC, 2.0) +
       1.45008e-2 * std::pow(TdegC, 3.0);
  dssdT = 4.70915e3 + 2.0 * (-4.87534) * TdegC +
          3.0 * 1.45008e-2 * std::pow(TdegC, 2.0);

  mob_res = M_H2O * ss * 1.e3 / 18.e-3 / 1.e-3 * 0.36;
  dsat = M_H2O * ss * 1.e3 / 18.e-3 / 1.e-3 * 1.2;
  dtemp = dssdT * M_H2O * 1.e3 / 18.e-3 / 1.e-3 * 0.36;
  assert(fabs((mob_l - mob_res) / mob_res) < eps);
  assert(fabs((ddXmobl.saturation[physics::Phase::liquid] - dsat) / dsat) <
         eps);
  assert(fabs(ddXmobl.saturation[physics::Phase::gas]) < eps);
  assert(fabs((ddXmobl.temperature - dtemp) / dtemp) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXmobl.pressure[alpha]) < eps);
  }
  for (auto &&alpha : physics::all_phases::array) {
    for (auto &&comp : physics::all_components::array) {
      assert(fabs(ddXmobl.molar_fractions[alpha][comp]) < eps);
    }
  }

  auto &&[mob_d, ddXmobd] = physics::sum_on_present_phases(
      physics::Context::diphasic, phase_mob_e[rocktypes(0)], coats_states(0));
  assert(fabs((mob_d - mob_g - mob_l) / mob_g) < eps);
  assert(
      fabs((ddXmobd.temperature - ddXmobg.temperature - ddXmobl.temperature) /
           ddXmobg.temperature) < eps);

  std::cout << "end of test " << std::endl;

  return 0;
}

inline auto test_closure_laws(pp::FMP_d &fluid_properties,
                              pp::RP_d &rock_properties,
                              const physics::State_field &coats_states,
                              const physics::Context_field &coats_contexts,
                              const physics::Rocktype_field &rocktypes) {
  auto eps = 1e-7;

  auto &&saturations = init_phase_saturation_functors();

  auto &&[sat_g, ddXsatg] = physics::sum_on_present_phases(
      physics::Context::gas, saturations, coats_states(1));

  assert(fabs(sat_g - 0.3) < eps);
  assert(fabs(ddXsatg.saturation[physics::Phase::gas] - 1.0) < eps);
  assert(fabs(ddXsatg.saturation[physics::Phase::liquid]) < eps);
  assert(fabs(ddXsatg.temperature) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXsatg.pressure[alpha]) < eps);
    for (auto &&comp : physics::all_components::array) {
      assert(fabs(ddXsatg.molar_fractions[alpha][comp]) < eps);
    }
  }

  auto &&[sat_l, ddXsatl] = physics::sum_on_present_phases(
      physics::Context::liquid, saturations, coats_states(1));

  assert(fabs(sat_l - 0.7) < eps);
  assert(fabs(ddXsatl.saturation[physics::Phase::liquid] - 1.0) < eps);
  assert(fabs(ddXsatl.saturation[physics::Phase::gas]) < eps);
  assert(fabs(ddXsatl.temperature) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXsatl.pressure[alpha]) < eps);
    for (auto &&comp : physics::all_components::array) {
      assert(fabs(ddXsatl.molar_fractions[alpha][comp]) < eps);
    }
  }

  // diphasic context
  auto &&[sat_d, ddXsatd] = physics::sum_on_present_phases(
      physics::Context::diphasic, saturations, coats_states(1));
  assert(fabs(sat_d - 1.0) < eps);
  assert(fabs(ddXsatd.temperature - ddXsatg.temperature - ddXsatl.temperature) <
         eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXsatd.saturation[alpha] - ddXsatg.saturation[alpha] -
                ddXsatl.saturation[alpha]) < eps);
    assert(fabs(ddXsatd.pressure[alpha] - ddXsatg.pressure[alpha] -
                ddXsatl.pressure[alpha]) < eps);
    for (auto &&ccomp : physics::all_components::array) {
      assert(fabs(ddXsatd.molar_fractions[alpha][ccomp] -
                  ddXsatg.molar_fractions[alpha][ccomp] -
                  ddXsatl.molar_fractions[alpha][ccomp]) < eps);
    }
  }

  auto &&mol_frac = init_phase_molar_fractions_functors();

  for (auto &&alpha : physics::all_phases::array) {
    auto &&phase_mol_frac = mol_frac.apply(alpha, coats_states(1));
    assert(fabs(sum(phase_mol_frac.value) - 1.0) < eps);
  }

  auto &&phase_fug = init_phase_fugacity_functors(fluid_properties);
  auto &&phase_fug_coef = init_phase_fugacity_coef_functors(fluid_properties);
  auto air_index = physics::component_index(physics::Component::air);
  auto water_index = physics::component_index(physics::Component::water);
  // gas fugacity
  auto &&fug_gas = phase_fug.apply(physics::Phase::gas, coats_states(1));
  assert(fabs(fug_gas.value[air_index] - 9.e4) < eps);
  assert(fabs(fug_gas.derivatives[air_index].pressure[physics::Phase::gas] -
              0.9) < eps);
  assert(fabs(fug_gas.derivatives[air_index]
                  .molar_fractions[physics::Phase::gas][water_index]) < eps);

  assert(fabs(fug_gas.value[water_index] - 1.e4) < eps);
  assert(fabs(fug_gas.derivatives[water_index].pressure[physics::Phase::gas] -
              0.1) < eps);
  assert(fabs(fug_gas.derivatives[water_index]
                  .molar_fractions[physics::Phase::gas][air_index]) < eps);

  for (auto &&comp : physics::all_components::array) {
    auto comp_index = physics::component_index(comp);
    assert(fabs(fug_gas.derivatives[comp_index].temperature) < eps);
    assert(
        fabs(fug_gas.derivatives[comp_index].pressure[physics::Phase::liquid]) <
        eps);
    assert(fabs(fug_gas.derivatives[air_index]
                    .molar_fractions[physics::Phase::liquid][comp]) < eps);
    assert(fabs(fug_gas.derivatives[comp_index]
                    .molar_fractions[physics::Phase::gas][comp_index] -
                1.e5) < eps);
    for (auto &&alpha : physics::all_phases::array) {
      assert(fabs(fug_gas.derivatives[comp_index].saturation[alpha]) < eps);
    }
  }

  // liquid fugacity
  auto &&fug_liq = phase_fug.apply(physics::Phase::liquid, coats_states(1));
  // air comp
  double ddHT = (10.e9 - 6.e9) / (353. - 293.);
  double H = 6.e9 + (350. - 293.) * ddHT;
  double fla = H * 0.2;
  assert(fabs((fug_liq.value[air_index] - fla) / fla) < eps); // scale norm
  assert(fabs(fug_liq.derivatives[air_index].temperature - 0.2 * ddHT) < eps);
  assert(fabs(fug_liq.derivatives[air_index]
                  .molar_fractions[physics::Phase::liquid][air_index] -
              H) < eps);
  assert(fabs(fug_liq.derivatives[air_index]
                  .molar_fractions[physics::Phase::liquid][water_index]) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(fug_liq.derivatives[air_index].pressure[alpha]) < eps);
    assert(fabs(fug_liq.derivatives[air_index].saturation[alpha]) < eps);
  }
  // water comp
  auto &&psat_f = init_psat(fluid_properties);
  auto &&psat = psat_f(coats_states(1));
  double deltap = 1.e5 - psat.value;
  double RTzeta = 8.314 * 350. * 1000.0 / 18.e-3;
  double beta = deltap / RTzeta;
  double dbetadp = 1.0 / RTzeta;
  double dbetadT =
      (-psat.derivatives.temperature - deltap * 1. / 350.) / RTzeta;
  double ebeta = exp(beta);
  double flw = psat.value * ebeta * 0.8;
  double DDflw_T =
      (psat.derivatives.temperature + psat.value * dbetadT) * ebeta * 0.8;
  double DDflw_pl = (psat.derivatives.pressure[physics::Phase::liquid] +
                     psat.value * dbetadp) *
                    ebeta * 0.8;
  double DDflw_cw = psat.value * ebeta;

  assert(fabs((fug_liq.value[water_index] - flw)) < eps);
  assert(fabs(fug_liq.derivatives[water_index].temperature - DDflw_T) < eps);
  assert(fabs(fug_liq.derivatives[water_index].pressure[physics::Phase::gas]) <
         eps);
  assert(
      fabs(fug_liq.derivatives[water_index].pressure[physics::Phase::liquid] -
           DDflw_pl) < eps);
  for (auto &&alpha : physics::all_phases::array)
    assert(fabs(fug_liq.derivatives[water_index].saturation[alpha]) < eps);
  for (auto &&comp : physics::all_components::array)
    assert(fabs(fug_liq.derivatives[water_index]
                    .molar_fractions[physics::Phase::gas][comp]) < eps);
  assert(fabs(fug_liq.derivatives[water_index]
                  .molar_fractions[physics::Phase::liquid][water_index] -
              DDflw_cw) < eps);
  assert(fabs(fug_liq.derivatives[water_index]
                  .molar_fractions[physics::Phase::liquid][air_index]) < eps);

  std::cout << "end of test " << std::endl;

  return 0;
}

inline void test_densities(pp::FMP_d &fluid_properties,
                           const physics::State_field &states) {
  auto eps = 1e-7;

  auto &&densities = init_phase_density_functors(fluid_properties);
  // liquid
  auto &&[res_dens_l, ddXres_dens_l] =
      densities.apply(physics::Phase::liquid, states(0));

  auto liq_dens = (0.8 * 18e-3 + 0.2 * 29e-3) * 1.e3 / 18.e-3;
  auto dclw_liq_dens = 18e-3 * 1.e3 / 18.e-3;
  auto dcla_liq_dens = 29e-3 * 1.e3 / 18.e-3;
  assert(fabs(res_dens_l - liq_dens) < eps);
  for (auto &&alpha : physics::all_phases::array) {
    assert(fabs(ddXres_dens_l.pressure[alpha]) < eps);
    assert(fabs(ddXres_dens_l.saturation[alpha]) < eps);
  }
  assert(fabs(ddXres_dens_l.temperature) < eps);
  for (auto &&comp : physics::all_components::array)
    assert(fabs(ddXres_dens_l.molar_fractions[physics::Phase::gas][comp]) <
           eps);
  assert(fabs(ddXres_dens_l.molar_fractions[physics::Phase::liquid]
                                           [physics::Component::water] -
              dclw_liq_dens) < eps);
  assert(fabs(ddXres_dens_l.molar_fractions[physics::Phase::liquid]
                                           [physics::Component::air] -
              dcla_liq_dens) < eps);

  // gas
  auto &&[res_dens_g, ddXres_dens_g] =
      densities.apply(physics::Phase::gas, states(0));

  auto gas_dens = (0.1 * 18e-3 + 0.9 * 29e-3) * 1.e5 / 8.314 / 300.;
  auto dtemp_gas_dens =
      -(0.1 * 18e-3 + 0.9 * 29e-3) * 1.e5 / 8.314 / std::pow(300, 2);
  auto dpg_gas_dens = (0.1 * 18e-3 + 0.9 * 29e-3) / 8.314 / 300;
  auto dclw_gas_dens = 18e-3 * 1.e5 / 8.314 / 300;
  auto dcla_gas_dens = 29e-3 * 1.e5 / 8.314 / 300;
  assert(fabs(res_dens_g - gas_dens) < eps);
  assert(fabs(ddXres_dens_g.temperature - dtemp_gas_dens) < eps);
  assert(fabs(ddXres_dens_g.pressure[physics::Phase::liquid]) < eps);
  assert(fabs(ddXres_dens_g.pressure[physics::Phase::gas] - dpg_gas_dens) <
         eps);
  for (auto &&alpha : physics::all_phases::array)
    assert(fabs(ddXres_dens_g.saturation[alpha]) < eps);
  for (auto &&comp : physics::all_components::array)
    assert(fabs(ddXres_dens_g.molar_fractions[physics::Phase::liquid][comp]) <
           eps);
  assert(fabs(ddXres_dens_g.molar_fractions[physics::Phase::gas]
                                           [physics::Component::water] -
              dclw_gas_dens) < eps);
  assert(fabs(ddXres_dens_g.molar_fractions[physics::Phase::gas]
                                           [physics::Component::air] -
              dcla_gas_dens) < eps);
}

} // namespace coats
