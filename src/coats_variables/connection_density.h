#pragma once

#include <icus/Field.h>
#include <icus/ISet.h>
#include <physics/model.h>

namespace coats {

template <typename Density> struct Connection_density {
  physics::Phase alpha;
  Density density;

  template <typename Flux_vector>
  inline auto average(icus::index_type s0, icus::index_type s1,
                      const icus::Field<physics::State> states,
                      const icus::Field<physics::Context> contexts) const {
    auto phases0 = physics::present_phases(contexts(s0));
    auto phases1 = physics::present_phases(contexts(s1));
    auto alpha_in_s0 =
        std::find(phases0.begin(), phases0.end(), alpha) != phases0.end();
    auto alpha_in_s1 =
        std::find(phases1.begin(), phases1.end(), alpha) != phases1.end();

    auto tips = std::array{s0, s1};
    auto alpha_in_tips = std::array{alpha_in_s0, alpha_in_s1};
    Flux_vector rho_average;
    auto compt = 0;
    rho_average.value = 0.0;
    for (auto &&[tip, alpha_in_tip] : std::views::zip(tips, alpha_in_tips)) {
      if (alpha_in_tip) {
        auto rho = density(states(tip));
        ++compt;
        rho_average.value += rho.value;
        // derivatives depend both on physical values and their location (tips)
        rho_average.derivatives.push_back(rho.derivatives);
      } else {
        rho_average.derivatives.push_back(physics::State{}); // 0.0 everywhere
      }
    }
    rho_average.value /= compt;
    return rho_average;
  }
};

} // namespace coats
