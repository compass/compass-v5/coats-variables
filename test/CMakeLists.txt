cmake_minimum_required(VERSION 3.24)
cmake_policy(VERSION 3.24)

project(coats-variables-tests LANGUAGES CXX)

execute_process(
  COMMAND compass use-compass-file
  OUTPUT_VARIABLE USE_COMPASS
  OUTPUT_STRIP_TRAILING_WHITESPACE)

include(${USE_COMPASS})

find_package(coats-variables REQUIRED)

compass_add_cpp_tests(FILES coats-variables.cpp sum-present-phases.cpp PRIVATE
                      coats-variables)
