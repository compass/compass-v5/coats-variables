import numpy as np
from icus import ISet, field
import physics
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)


def test_cpp_densities():
    from coats_variables import test_densities

    # load the physics
    diphasic_physics = physics.load("diphasic")
    # load the default physical properties of the diphasic physics
    the_fluid_properties = init_default_fluid_physical_properties(diphasic_physics)

    n_sites = 2
    coats_sites = ISet(n_sites)
    coats_states = field(coats_sites, dtype=diphasic_physics.State)

    # init coats_states
    gas = diphasic_physics.Phase.gas
    liquid = diphasic_physics.Phase.liquid
    water = diphasic_physics.Component.water
    air = diphasic_physics.Component.air

    arr_states = coats_states.as_array()
    arr_states.temperature[:] = 300.0
    arr_states.saturation[:, gas] = 0.4
    arr_states.saturation[:, liquid] = 1.0 - arr_states.saturation[:, gas]
    arr_states.molar_fractions[:, gas, water] = 0.1
    arr_states.molar_fractions[:, liquid, water] = 0.8
    for alpha in range(diphasic_physics.nb_phases):
        arr_states.molar_fractions[:, alpha, air] = (
            1.0 - arr_states.molar_fractions[:, alpha, water]
        )
        arr_states.pressure[:, alpha] = 1.0e5

    test_densities(
        the_fluid_properties.cpp_prop_with_derivatives,
        coats_states,
    )


if __name__ == "__main__":
    test_cpp_densities()
