from icus import ISet, field
import physics


def test_cpp_units_variables():
    from coats_variables import test_units_variables

    diph_physics = physics.load("diphasic")

    n_sites = 2
    coats_sites = ISet(n_sites)
    coats_states = field(coats_sites, dtype=diph_physics.State)

    arr_states = coats_states.as_array()
    arr_states.temperature[:] = 300.0
    arr_states.saturation[diph_physics.Phase.gas] = 0.4
    arr_states.saturation[diph_physics.Phase.liquid] = (
        1.0 - arr_states.saturation[diph_physics.Phase.gas]
    )
    arr_states.molar_fractions[
        diph_physics.Phase.gas, diph_physics.Component.water
    ] = 0.1
    arr_states.molar_fractions[
        diph_physics.Phase.liquid, diph_physics.Component.water
    ] = 0.8
    for alpha in range(diph_physics.nb_phases):
        arr_states.molar_fractions[alpha, diph_physics.Component.air] = (
            1.0 - arr_states.molar_fractions[alpha, diph_physics.Component.water]
        )
        arr_states.pressure[alpha] = 1.0e5

    test_units_variables(coats_states)

    # test FieldAccumulation bindings
    coats_acc = field(coats_sites, dtype=diph_physics.Accumulation)
    assert coats_acc.as_array().energy.shape[0] == n_sites
    assert coats_acc.as_array().molar.shape[0] == n_sites
    assert coats_acc.as_array().molar.shape[1] == diph_physics.nb_components


if __name__ == "__main__":
    test_cpp_units_variables()
