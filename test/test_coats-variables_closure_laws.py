import numpy as np
from icus import ISet, field, labels
import physics
from physicalprop.fluid_physical_properties import (
    init_default_fluid_physical_properties,
)
from physicalprop.rock_physical_properties import init_default_rock_physical_properties


def test_closure_laws_default_prop():
    from coats_variables import test_closure_laws

    # load the physics
    diphasic_physics = physics.load("diphasic")
    # load the default physical properties of the diphasic physics
    the_fluid_properties = init_default_fluid_physical_properties(diphasic_physics)
    the_rock_properties = init_default_rock_physical_properties(diphasic_physics)

    n_sites = 2
    coats_sites = ISet(n_sites)
    coats_states = field(coats_sites, dtype=diphasic_physics.State)
    contexts = field(coats_sites, dtype=diphasic_physics.Context)
    rocktypes = labels(coats_sites)

    # init coats_states
    gas = diphasic_physics.Phase.gas
    liquid = diphasic_physics.Phase.liquid
    water = diphasic_physics.Component.water
    air = diphasic_physics.Component.air

    arr_states = coats_states.as_array()
    arr_states.temperature[:] = 350.0
    arr_states.saturation[:, gas] = 0.3
    arr_states.saturation[:, liquid] = 1.0 - arr_states.saturation[:, gas]
    arr_states.molar_fractions[:, gas, water] = 0.1
    arr_states.molar_fractions[:, liquid, water] = 0.8
    for alpha in range(diphasic_physics.nb_phases):
        arr_states.molar_fractions[:, alpha, air] = (
            1.0 - arr_states.molar_fractions[:, alpha, water]
        )
        arr_states.pressure[:, alpha] = 1.0e5

    # careful: here Context is expected, no Phase
    contexts[...] = diphasic_physics.Context.diphasic

    # init rocktypes
    rocktypes[...] = 0

    test_closure_laws(
        the_fluid_properties.cpp_prop_with_derivatives,
        the_rock_properties.cpp_prop_with_derivatives,
        coats_states,
        contexts,
        rocktypes,
    )


if __name__ == "__main__":
    test_closure_laws_default_prop()
