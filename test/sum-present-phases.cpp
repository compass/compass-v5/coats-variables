#include <coats_variables/coats-variables.h>
#include <icus/ISet.h>

namespace coats {

template <typename State_variable> struct _Phase_sat { // one by phase
  const physics::State_field states;
  State_variable saturation; // pscal function of State

  auto operator()(const auto site) const {
    // returns [val, ddX]
    return saturation(states(site));
  }
};

auto init_phase_satcomp_functor() {
  auto gas_var =
      pp::make_variable_IdState(_saturation<physics::Phase::gas>) *
      pp::make_vect_variable_IdState(_molar_fractions<physics::Phase::gas>);
  auto liq_var =
      pp::make_variable_IdState(_saturation<physics::Phase::liquid>) *
      pp::make_vect_variable_IdState(_molar_fractions<physics::Phase::liquid>);
  return physics::phase_functors(gas_var, liq_var);
}

auto init_phase_sat_struct(const physics::State_field &coats_states) {
  auto gas_var =
      _Phase_sat{coats_states,
                 pp::make_variable_IdState(_saturation<physics::Phase::gas>)};
  auto liq_var = _Phase_sat{
      coats_states,
      pp::make_variable_IdState(_saturation<physics::Phase::liquid>)};
  return physics::phase_functors(gas_var, liq_var);
}

void test_sum_on_present_phases(const physics::State_field &coats_states,
                                const physics::Context_field &coats_contexts,
                                const physics::Scalar_field &porosity) {

  // step 1 : use pscal functions
  auto &&phase_satcomp = init_phase_satcomp_functor();
  auto &&[Sc, ddSc] = physics::sum_on_present_phases(
      coats_contexts(0), phase_satcomp, coats_states(0));
  for (auto &&toto : Sc) {
    std::cout << "Sum of sat * comp is " << toto << " ddS[0].temp "
              << ddSc[0].temperature << std::endl;
  }
  // step 2:
  // use struct called on site then the struct calls pscal(states(site))
  auto &&phase_sat_struct = init_phase_sat_struct(coats_states);
  auto &&[Sgas, ddSgas] = phase_sat_struct.apply(physics::Phase::gas, 0);
  std::cout << "Sat is " << Sgas << " ddSgas.temp " << ddSgas.temperature
            << std::endl;
  auto &&[S, ddS] =
      physics::sum_on_present_phases(coats_contexts(0), phase_sat_struct, 0);
  std::cout << "Sum of sat is " << S << " ddS.temp " << ddS.temperature
            << std::endl;
}
} // namespace coats

int main() {
  using namespace coats;

  int n_sites = 2;
  auto coats_sites = icus::ISet(n_sites);
  auto coats_states = physics::State_field(coats_sites);
  auto porosity = physics::Scalar_field(coats_sites);
  auto contexts = physics::Context_field(coats_sites);

  auto xcoats = physics::State{};
  xcoats.temperature = 300.0;
  xcoats.molar_fractions[0][0] = 0.1;
  xcoats.molar_fractions[0][1] = 0.9;
  xcoats.molar_fractions[1][0] = 0.8;
  xcoats.molar_fractions[1][1] = 0.2;
  xcoats.saturation[0] = 0.4;
  xcoats.saturation[1] = 0.6;
  for (int alpha = 0; alpha < physics::nb_components; ++alpha) {
    xcoats.pressure[alpha] = 1.0e5;
  }

  // init coats_states and porosity with coherent values
  coats_states.fill(xcoats);
  porosity.fill(0.15);
  contexts.fill(physics::Context::gas);

  test_sum_on_present_phases(coats_states, contexts, porosity);

  return 0;
}
